### Contributing

Any kind of contribution is welcome. It can be one of the following contribution categories:

- issues reporting,
- feature requests,
- pull requests (code contribution),
- documentation and other text contribution,
- anything else.

#### Branches

Currently, I have chosen to keep the main branch called `master`. This branch is a development branch, also it contains some stable portions. Any other branch is currently a short-lived feature branch, which I use to fork the `master`, do some edits and after I am happy with the changes, I merge them to the `master`.

However, this might (and probably will) change in the future.

#### Commit Messages

All commit messages must be written in English, preferably in British English (though that is not a must) and it should use the proper grammer (although I know neither my English is perfect).

#### EditorConfig

As you might notice, I added [`.editorconfig`](.editorconfig) file, using which I require that all text files:

- use `lf` line endings (i.e. a Unix/Linux ones, not those of MS Windows or macOS);
- use UTF-8 character encoding;
- there should be not trailing whitespace (which is automatically trimmed, when the `.editorconfig` is used by your text editor);
- there should be no final newline (and therefore the files are not POSIX-compliant)
- use tab indentation of size of 2 characters.