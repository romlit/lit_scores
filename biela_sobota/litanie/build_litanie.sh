#!/bin/bash
cwd="$PWD"
if [ -d out_litanie ]; then
	lilypond-book --output=out_litanie --pdf --latex-program=xelatex litanie.lytex && cd out_litanie && xelatex litanie.tex
else
	lilypond-book --output=out_litanie --pdf --latex-program=xelatex litanie.lytex && cd out_litanie && xelatex litanie.tex && xelatex litanie.tex
fi
cp litanie.pdf ../
cd "$cwd"