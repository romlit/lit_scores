### Changelog

| Start Date  |  End Date   |              Description               |   Status   |
| :---------: | :---------: | :------------------------------------- | :--------- |
| 20 Apr 2019 | TBD         | Add the Passion scores for Good Friday | Unfinished |
| 20 Apr 2019 | 20 Apr 2019 | Add the Solemn Prayers of the Faithful | Usable     |
| 20 Apr 2019 | 20 Apr 2019 | Add the Litanies on Holy Saturday      | Finished   |